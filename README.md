# NextGrowth Labs
#### Screening evaluation Project

This evaluation project is done for the **NextGrowth Labs** recruitment process.


## Problem Set 1


``` python
import re

string = '{"orders":[{"id":1},{"id":2},{"id":3},{"id":4},{"id":5},{"id":6},{"id":7},{"id":8},{"id":9},{"id":10},{"id":11},{"id":648},{"id":649},{"id":650},{"id":651},{"id":652},{"id":653}],"errors":[{"code":3,"message":"[PHP Warning #2] count(): Parameter must be an array or an object that implements Countable (153)"}]}'

print(re.findall(r'(?<=:)\d+', string))

```

``` output
List:- ['1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '648', '649', '650', '651', '652', '653']
```


## Problem Set 2

### Description

The aim of the project is to build a Django app, in which admin is able to add a new app with it's details like url, category, subcategory, logo and points for download. And a user should be able to view the apps available, follow the link, earn points by saving a screen shot of the app and view points earned.

### Video demo

NB: Follow this [link](https://screenrec.com/share/VkztRU6yBc) for a video demo of the app.


### Technologies used


*	`Django`
    > Django framework is used for the app creation as it is simple and have most of the essential extensions inbuilt.

*	`Django Rest Framework`
    > Used for implimenting APIs.

*	`Vue3`
    > Used as a JavaScript framework for building UI & UX. Inside vue.js used:
    > - Vue Router as router for making it a single page application

*	`SQlite`
    > As the database For Development.

*   `PostgresSql`
    >As the database For Production Environment.



### DB Schema Design

### Entity-Relationship (ER) Diagram


The database has three tables:

#### App Table:

- **`app_name`:**
  - A field storing the name of the application.

- **`points`:**
  - An integer field representing the points associated with the application.

- **`app_category`:**
  - An integer field with choices representing the category of the application.

- **`app_image`:**
  - A URL field allowing storage of an image associated with the application.

- **Created and updated timestamps (`created_at`, `updated_at`):**
  - Inherited from the BaseModel.

- **Unique Constraint:**
  - The `app_name` field has a unique constraint, ensuring that each application has a distinct name.

#### Customer Table:

- **Inherits from:**
  - The built-in Django User model and the BaseModel abstract model.

- **`gender`:**
  - A character field representing the gender of the customer.

- **`date_of_birth`:**
  - A date field storing the birthdate of the customer.

- **`points_earned`:**
  - An integer field representing the total points earned by the customer.

- **`tasks_completed`:**
  - An integer field representing the total tasks completed by the customer.

- **`is_admin`:**
  - A boolean field indicating whether the customer has administrative privileges.

- **`user_image`:**
  - A URL field allowing storage of a profile image for the customer.

- **Created and updated timestamps (`created_at`, `updated_at`):**
  - Inherited from the BaseModel.

- **Unique Constraint:**
  - The `username` field of the User model has a unique constraint, ensuring that each customer has a unique username.

#### AppCustomer Table:

- **Represents:**
  - A many-to-many relationship between Customer and App models.

- **`user`:**
  - A foreign key referencing the Customer model with the CASCADE option, ensuring that if a customer is deleted, all associated AppCustomer records are also deleted.

- **`app`:**
  - A foreign key referencing the App model with the CASCADE option, ensuring that if an application is deleted, all associated AppCustomer records are also deleted.

- **`screenshot`:**
  - A URL field allowing storage of screenshots associated with the AppCustomer relationship.

- **Created and updated timestamps (`created_at`, `updated_at`):**
  - Inherited from the BaseModel.

- **Unique Constraint:**
  - The combination of the `user` and `app` foreign keys has a unique constraint, ensuring that each customer can only have one entry per application in the AppCustomer table.

- **Cascade Option:**
  - Each relationship in the database has the CASCADE option set, meaning that if a parent object (either a Customer or an App) is deleted, all its related child objects in the AppCustomer table will be deleted as well. This ensures data consistency and avoids orphaned records in the database.



### API Design

# API Documentation

For detailed documentation and interactive exploration of our API, please visit the Swagger documentation page:

[API Swagger Documentation](https://nextlab-app.onrender.com/)

Explore the available endpoints, test requests, and understand how to interact with our API. If you have any questions or need assistance, feel free to reach out.

# Additional Resources

- [Google Sheets](https://docs.google.com/spreadsheets/d/1lAU_3EdB34XI-fT8XbeEdDnT8_CCbxzkwPkFn6RLoDc/edit#gid=0) - Access our Google Sheets document for additional data and information.

Feel free to use the provided links to access the Swagger documentation and Google Sheets. If you have any questions or need further clarification, don't hesitate to contact us.


### Local Development Run

## Installation

Clone the repository:

```bash
git clone https://gitlab.com/nextlab3405645/nestlabsevaluation.git

Navigate to the project directory:
cd nestlabsevaluation

Create and activate a virtual environment:
python -m venv venv
source venv/bin/activate   # On Windows, use venv\Scripts\activate

Install dependencies:
pip install -r requirements.txt

Configure the .env file:

Create a file named .env in the root of your project and add the necessary environment variables. For example:
SECRET_KEY=your_secret_key
DEBUG=True
DATABASE_URL=your_database_url

Replace your_secret_key and your_database_url with your actual secret key and database URL.

Run migrations:
python manage.py makemigrations app
python manage.py migrate

Open your browser and navigate to http://localhost:8000/ to access the local development environment.


Make sure to replace `your_secret_key` and `your_database_url` with your actual secret key and database URL. Additionally, ensure that the environment variables in the .env file match the configuration needed for your Django project.
```

# Deployment Process

## Overview

The deployment process for our project, utilizing Render PaaS for hosting the backend REST API, Vue 3 frontend, and PostgreSQL database. AWS S3 is used to store images for `user_image`, `app_image`, and `screenshot` fields.


## Backend Deployment (REST API)

### Prerequisites

- [Render Account](https://render.com/)
- Backend codebase ready for deployment
- `build.sh` script for deployment configuration

### Steps

1. **Clone the Repository:**

    ```bash
    git clone https://gitlab.com/nextlab3405645/nestlabsevaluation.git
    cd nestlabsevaluation
    ```

2. **Backend Setup:**

    - Ensure that the backend code is ready for deployment.
    - Create a `.env` file with the necessary environment variables.

3. **Build Configuration:**

    - Create a `build.sh` script with deployment commands, including Gunicorn setup.
  
    Example `build.sh`:

    ```bash
    #!/bin/bash

    # Install dependencies
    pip install -r requirements.txt

    # Run database migrations
    python manage.py migrate

    # Collect static files
    python manage.py collectstatic --noinput
    ```

4. **Manual Deployment on Render:**

    - Log in to your Render account.
    - Create a new web service for the backend.
    - Configure environment variables (e.g., `DATABASE_URL`, `SECRET_KEY`, `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, `AWS_STORAGE_BUCKET_NAME`).
    - Specify the start command: `./build.sh && gunicorn task.wsgi:application -b 0.0.0.0:8000`.
    - Set the build command: `./build.sh`.
    - Deploy the backend service.

    Note: Make sure to include the necessary AWS S3 credentials (`AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, `AWS_STORAGE_BUCKET_NAME`) for image storage.

## Frontend Deployment (Vue 3)

### Prerequisites

- Backend API URL (from Render or other deployment)
- Vue 3 frontend codebase ready for deployment

### Steps

1. **Clone the Repository:**

    ```bash
    git clone https://gitlab.com/nextlab3405645/nextappvuefrontend.git
    cd nestlabsevaluation-frontend
    ```

2. **Frontend Setup:**

    - Ensure that the frontend code is ready for deployment.
    - Update API URLs in the codebase with the deployed backend URL.

3. **Manual Deployment on Render (Static Site):**

    - Log in to your Render account.
    - Create a new static site for the frontend.
    - Deploy the frontend code.

## Database Deployment (PostgreSQL on Render)

1. **Create a Render Database:**

    - Log in to your Render account.
    - Create a new PostgreSQL database.
    - Configure environment variables for database connection.

2. **Configure Backend with Database URL:**

    - Update the `.env` file in the backend with the Render PostgreSQL database URL.

## AWS S3 for Image Storage

Images for `user_image`, `app_image`, and `screenshot` fields are stored on AWS S3. The AWS S3 credentials (`AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, `AWS_STORAGE_BUCKET_NAME`) should be configured as environment variables for the backend service during deployment.

## Conclusion

Congratulations! Your project is now deployed on Render with a backend REST API, Vue 3 frontend, and PostgreSQL database. Images are stored on AWS S3. For any issues or further customization, refer to Render's documentation, AWS S3 documentation, or reach out for support.



## Problem Set 3

#### Write and share a small note about your choice of system to schedule periodic tasks (such as downloading a list of ISINs every 24 hours). Why did you choose it? Is it reliable enough; Or will it scale? If not, what are the problems with it? And, what else would you recommend to fix this problem at scale in production?

`
# Scheduling Periodic Tasks: Choosing the Right Approach

## Introduction

For scheduling periodic tasks like downloading a list of ISINs every 24 hours, two robust approaches stand out. This guide explores three combined approaches: Celery with Redis, AWS Lambda with CloudWatch Events, and traditional Cron Jobs.

## Considerations

### Combined Approach: Celery with Redis, AWS Lambda with CloudWatch Events, and Cron Jobs

**Reliability:**

- **Celery with Redis:**
  - Offers excellent reliability with task retries, prioritization, and monitoring.
  
- **AWS Lambda:**
  - Built for high availability and reliability, with automatic scaling and error handling.
  
- **Cron Jobs:**
  - Well-established and reliable for periodic tasks, but lacks some features compared to Celery and Lambda.

**Scalability:**

- **Celery with Redis:**
  - Highly scalable by adding more worker nodes; Redis efficiently handles message throughput.

- **AWS Lambda:**
  - Inherently scalable, automatically adjusting to workload changes.

- **Cron Jobs:**
  - Limited scalability; may require manual intervention to handle increased loads.

**Simplicity and Cost Efficiency:**

- **Celery with Redis:**
  - Proven simplicity and flexibility, though maintenance can be complex at scale.

- **AWS Lambda:**
  - Serverless simplicity, reducing operational overhead, and cost-efficient with pay-as-you-go pricing.

- **Cron Jobs:**
  - Simple and cost-effective, but lacks some automation features of Celery and Lambda.

### Potential Challenges and Solutions

**Maintenance and High Availability:**

- **Celery with Redis:**
  - Requires careful setup and ongoing management; implement redundancy and failover mechanisms.

- **AWS Lambda:**
  - Address potential cold starts by optimizing code; ensure proper configuration for resource limits.

- **Cron Jobs:**
  - May need additional efforts for ensuring high availability; consider redundant setups and error handling.

**Scaling at Production Scale:**

- **Celery with Redis:**
  - Consider containerization for portability and easier scaling; implement auto-scaling policies.

- **AWS Lambda:**
  - Optimize code for efficiency and use CloudWatch Events for scheduled triggers.

- **Cron Jobs:**
  - Limited scaling options; may require distributing tasks across multiple servers manually.

**Logging and Monitoring:**

- Implement comprehensive logging and monitoring using tools like Prometheus, Grafana, CloudWatch Logs, and Alarms for all three solutions.

## Simplified Version

For scheduling tasks like daily ISIN downloads, a combination of Celery with Redis, AWS Lambda with CloudWatch Events, and Cron Jobs offer different trade-offs. Celery provides reliability and flexibility, AWS Lambda offers simplicity and scalability, and Cron Jobs are simple and cost-effective. Optimize code, implement proper logging and monitoring, and choose the solution that aligns best with your specific needs and constraints.

`
#### In what circumstances would you use Flask instead of Django and vice versa?

# Choosing Between Flask and Django

## Use Flask When:

- **Microservices and Small Apps:**
  - For small to medium-sized applications or microservices with a need for control and flexibility.

- **Minimalistic and Lightweight:**
  - If you prefer a minimalistic, lightweight framework following the "use what you need" philosophy.

- **RESTful APIs:**
  - Well-suited for building clean, simple, and flexible RESTful APIs.

- **Rapid Prototyping:**
  - Ideal for quick prototyping and development due to simplicity and minimal boilerplate code.

- **Customization:**
  - Provides more freedom for customization based on specific requirements.

## Use Django When:

- **Full-Stack Development:**
  - A full-stack framework with built-in features, suitable for complete web applications with minimal effort.

- **Rapid Development:**
  - If you need to develop a web application quickly, thanks to its "batteries-included" approach.

- **Large and Complex Applications:**
  - Best for large and complex applications with features that benefit from Django's organization and structure.

- **Content Management Systems (CMS):**
  - Powerful for building content management systems and applications requiring data management and CRUD operations.

- **Security:**
  - Django comes with built-in security features, protecting against common web vulnerabilities.

### Features of Django:

- **Flexibility:**
  - Suitable for building any website with content in various formats.

- **Safety:**
  - Handles user account management, transaction management, security features automatically.

- **Scalability:**
  - Designed to handle high traffic demands in large web applications.

- **Full-Stack:**
  - Offers a wide range of features for full-stack web development.

### Disadvantages of Django:

- **Compatibility Issues:**
  - With some technologies.

- **Code Size:**
  - Can be large.

- **Single Request Handling:**
  - Handles one request at a time.

### Features of Flask:

- **Adaptability:**
  - More adaptable to different working styles and approaches.

- **Database Support:**
  - Supports multiple types of databases by default.

- **Scalability:**
  - Easily scalable for smaller web applications.

- **Development Server:**
  - Comes with a built-in development server and fast debugger.

### Disadvantages of Flask:

- **Limited Support:**
  - In comparison to Django.

- **Smaller Community:**
  - May have a smaller community.

- **Maintenance:**
  - Larger implementations may require more complicated maintenance.

## Flask vs Django - Similarities:

- Both are open-source Python web frameworks.
- Well-established, mature communities.
- Popular, widely supported, and enhance programming productivity.

## When to Use Django or Flask:

### Use Django When:

- Working on large projects with deadlines.
- Scaling up to more complex tasks.
- Needing active online community support.
- Creating web apps with ORM support, API backends, and plans for high-end technologies.

### Use Flask When:

- Working on smaller projects with coding flexibility.
- Needing API support and planning to include more extensions.
- Wanting more control over the database.
- Creating static websites, rapid prototypes, MVPs, and RESTful web services.

>>  - Project Repo Link     :- <https://gitlab.com/nextlab3405645/nestlabsevaluation.git>
>>  - Frontend Repo Lik     :- <https://gitlab.com/nextlab3405645/nextappvuefrontend>
>>  - WebApp Link (frontend):- <https://nextapp-96bx.onrender.com>
>>  - Backendswagger Link   :- <https://nextlab-app.onrender.com>
>>  - BackendApi's Base EndPoint :- <https://nextlab-app.onrender.com/api/>
